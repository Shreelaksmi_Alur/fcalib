/**
 * Test implementation for testing attribute exploration in partial contexts.
 * @author Baris Sertkaya
 */
package statement_coverage;

import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.action.StartExplorationAction;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;

/*
 * FCAlib: An open-source extensible library for Formal Concept Analysis 
 *         tool developers
 * Copyright (C) 2009  Baris Sertkaya
 *
 * This file is part of FCAlib.
 * FCAlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCAlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCAlib.  If not, see <http://www.gnu.org/licenses/>.
 */

public class TestPartialContext extends TestCase {

	public TestPartialContext() {
	}

	public void testPartialContext() {
		BasicConfigurator.configure();

		PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
		NoExpertPartial<String> expert = new NoExpertPartial<>(context);

		context.addAttribute("a");
		context.addAttribute("b");
		context.addAttribute("c");
		context.addAttribute("d");
		System.out.println("Attributes: " + context.getAttributes());

		expert.addExpertActionListener(context);
		context.setExpert(expert);

		StartExplorationAction<String, String, PartialObject<String, String>> action = new StartExplorationAction<>();
		action.setContext(context);
		expert.fireExpertAction(action);
	}
	
	@Test
	public void testAddAndRemoveObjectMethods() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		context.getObjectAtIndex(0);
		context.getObjectCount();
		context.removeObject(o);
		context.clearObjects();
	}
	
	@Test
	public void testGetObject(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		o.getIdentifier().equals("id");
		context.getObject("id");
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testRemoveObjectIfNotRemoved(){
		PartialContext context = new PartialContext();
		PartialObject id =  new PartialObject("id");
		try {
			context.removeObject(id);
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveObjectWithId() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		String id = "";
		PartialObject o = new PartialObject(id);
		context.addObject(o);
		context.removeObject(id);
	}
	
	@Test(expected = IllegalAttributeException.class)
	public void testAddAttributeToObjectDoesntContainsAttribute(){
		PartialContext context = new PartialContext();
		Object attribute = "attr";
		Object id = "id";
		try {
			context.addAttributeToObject(attribute, id);
		} catch (IllegalAttributeException | IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalAttributeException.class)
	public void testAddAttributeToObjectDoesntContainsId(){
		PartialContext context = new PartialContext();
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		try {
			context.addAttributeToObject(attribute,id);
		} catch (IllegalAttributeException | IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddAndRemoveAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
		context.removeAttributeFromObject(attribute, id);
	}
	
	@Test(expected = IllegalAttributeException.class)
	public void testAddAttributeToObjectContainsAttribute(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		o.getDescription().addAttribute(attribute);
		context.addAttribute(attribute);
		try {
			context.addAttributeToObject(attribute,id);
		} catch (IllegalAttributeException | IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expected = IllegalAttributeException.class)
	public void testRemoveAttributeFromObjectDoesntContainAttribute(){
		PartialContext context = new PartialContext();
		Object attribute = "attr";
		Object id = "id";
		try {
			context.removeAttributeFromObject(attribute, id);
		} catch (IllegalAttributeException | IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected=IllegalObjectException.class)
	public void testRemoveAttributeFromObjectDoesntContainsId(){
		PartialContext context = new PartialContext();
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		try {
			context.removeAttributeFromObject(attribute, id);
		} catch (IllegalAttributeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalAttributeException.class)
	public void testRemoveAttributeToObjectContainsAttribute(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		try {
			context.removeAttributeFromObject(attribute, id);
		} catch (IllegalAttributeException | IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testObjectHasAttribute(){
		PartialContext context = new PartialContext();
		PartialObject obj = new PartialObject("");
		Object attribute = "attr";
		context.objectHasAttribute(obj, attribute);
	}
	
	@Test
	public void testObjectHasNegatedAttribute(){
		PartialContext context = new PartialContext();
		PartialObject obj = new PartialObject("");
		Object attribute = "attr";
		context.objectHasNegatedAttribute(obj, attribute);
	}
	
	@Test
	public void testGetBases(){
		PartialContext context = new PartialContext();
		context.getStemBase();
		context.getDuquenneGuiguesBase();
	}
	
	@Test
	public void testExistingObject() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		context.addObject(o);
	}
	
	@Test
	public void testRefutes(){
		PartialContext context = new PartialContext();
		FCAImplication imp = null;
		context.refutes(imp);
	}
	
}
