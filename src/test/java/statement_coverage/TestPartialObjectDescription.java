package statement_coverage;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;


import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;

public class TestPartialObjectDescription {
	
	@Test
	public void testAddAttribute(){
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddAttributesException(){
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addNegatedAttribute(attribute);
		context.addAttribute(attribute);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddNegatedAttributesException(){
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
		context.addNegatedAttribute(attribute);
	}

	@Test
	public void testObjectClone() throws CloneNotSupportedException {
		PartialObjectDescription context = new PartialObjectDescription();
		context.clone();
	}
	
	@Test
	public void testNegatedAttribute(){
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addNegatedAttribute(attribute);
		context.containsNegatedAttribute(attribute);
	}
	
	@Test
	public void testNegatedAttributes(){
		PartialObjectDescription context = new PartialObjectDescription();
		Set attrs = new HashSet<>();
		context.addNegatedAttribute(attrs);
		context.containsNegatedAttributes(attrs);
	}
	
}
