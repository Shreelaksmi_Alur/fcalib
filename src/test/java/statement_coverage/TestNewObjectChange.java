package statement_coverage;

import static org.junit.Assert.*;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.change.NewObjectChange;

public class TestNewObjectChange<A> {
	private Context<A,?,FCAObject<A,?>> context;
	private FCAObject<A,?> object;

	@Test
	public void testNewObjectChange() {
		NewObjectChange objChange = new NewObjectChange(context,object);
		objChange.getObject();
		objChange.getType();
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testNewObjectChangeUndo() {
		NewObjectChange objChange = new NewObjectChange(context,object);
		objChange.getObject();
	}

}
