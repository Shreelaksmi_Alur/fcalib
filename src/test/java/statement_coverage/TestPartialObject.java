package statement_coverage;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.PartialObject;

public class TestPartialObject {

	@Test
	public void test() {
		PartialObject context = new PartialObject("id");
		context.setName("yoda");
		context.getName();
	}

	@Test
	public void testPartialObjectConstructor() {
		Set attrs = new HashSet<>();
		Set negatedAttrs = new HashSet<>();
		PartialObject context = new PartialObject("id",attrs,negatedAttrs);
	}
	
	@Test
	public void testRefutes() {
		//PartialObject context = new PartialObject("id");
		//FCAImplication<> imp = new FCAImplication<A>();
		//context.respects(imp);
	}
}
