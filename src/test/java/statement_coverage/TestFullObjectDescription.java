package statement_coverage;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import de.tudresden.inf.tcs.fcalib.FullObjectDescription;

public class TestFullObjectDescription {

	@Test
	public void testFullObjectDescription() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
		context.removeAttribute(attribute);
	}
	
	@Test
	public void testAddAttributes() {
		FullObjectDescription context = new FullObjectDescription();
		Set attrs = new HashSet<>();
		context.addAttributes(attrs);
	}

	@Test
	public void testObjectClone() throws CloneNotSupportedException {
		FullObjectDescription context = new FullObjectDescription();
		context.clone();
	}
}
