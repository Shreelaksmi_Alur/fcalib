package statement_coverage;

import static org.junit.Assert.*;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAObject;
import de.tudresden.inf.tcs.fcalib.change.ObjectHasAttributeChange;

public class TestObjectHasAttributeChange<A> {
	
	private FCAObject<A,?> object;
	private A attribute;

	@Test
	public void testObjectHasAttributeChange() {
		ObjectHasAttributeChange attrChange = new ObjectHasAttributeChange(object,attribute);
		attrChange.getObject();
		attrChange.getAttribute();
		attrChange.getType();
	}

}
