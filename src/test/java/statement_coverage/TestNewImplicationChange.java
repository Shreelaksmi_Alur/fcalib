package statement_coverage;

import static org.junit.Assert.*;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.Context;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.change.NewImplicationChange;

public class TestNewImplicationChange<A> {
	private FCAImplication<A> implication;
	private Context<A,?,?> context;

	@Test
	public void testNewImpChange() {
		NewImplicationChange impChange = new NewImplicationChange(context,implication);
		impChange.getImplication();
		impChange.getType();
	}

}
