package statement_coverage;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.change.ContextChange;
import de.tudresden.inf.tcs.fcalib.change.HistoryManager;

public class TestHistoryManager<A,B extends ContextChange<A>> extends ArrayList<ContextChange<A>> {

	@Test
	public void testPush() {
		HistoryManager manager = new HistoryManager();
		ContextChange change = null;
		manager.push(change);
	}
	
	@Test
	public void testUndoAll() {
		HistoryManager manager = new HistoryManager();
		manager.undoAll();
	}
}
