package statement_coverage;

import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.Expert;
import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcaapi.utils.IndexedSet;
import de.tudresden.inf.tcs.fcalib.AbstractContext;
import de.tudresden.inf.tcs.fcalib.FullObject;

public class AbstractContextModel<A,I> extends AbstractContext<A,I,FullObject<A,I>>{

//	@Test
//	public void test() {
//		AbstractContext context = new AbstractContext();
//	}

	@Override
	public FullObject<A, I> getObject(I id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FullObject<A, I> getObjectAtIndex(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean objectHasAttribute(FullObject<A, I> object, A attribute) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<FCAImplication<A>> getDuquenneGuiguesBase() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Expert<A, I, FullObject<A, I>> getExpert() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setExpert(Expert<A, I, FullObject<A, I>> e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public IndexedSet<FullObject<A, I>> getObjects() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addObject(FullObject<A, I> object) throws IllegalObjectException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeObject(I id) throws IllegalObjectException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeObject(FullObject<A, I> object) throws IllegalObjectException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAttributeToObject(A attribute, I id) throws IllegalAttributeException, IllegalObjectException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<A> doublePrime(Set<A> x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<FCAImplication<A>> getStemBase() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean refutes(FCAImplication<A> imp) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCounterExampleValid(FullObject<A, I> counterExample, FCAImplication<A> imp) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean followsFromBackgroundKnowledge(FCAImplication<A> implication) {
		// TODO Auto-generated method stub
		return false;
	}

}
