package statement_coverage;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;

public class TestFullObject {

	@Test
	public void testFullObject() {
		FullObject context = new FullObject("id");
		context.setName("yoda");
		context.getName();
	}
	
//	@Test
//	public void testRespect(FCAImplication imp){
//		FullObject context = new FullObject("id");
//		imp = null;
//		context.respects(imp);
//		context.refutes(imp);
//	}
	
}
