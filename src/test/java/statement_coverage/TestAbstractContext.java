package statement_coverage;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FullObject;

public class TestAbstractContext {

	@org.junit.Test
	public void testAbstractContext() {
		AbstractContextModel model = new AbstractContextModel();
		Set attrs = new HashSet();
		model.addAttributes(attrs);
		model.getImplications();
		model.getCurrentQuestion();
		model.isExpertSet();
	}

	@org.junit.Test(expected = IllegalObjectException.class)
	public void testAbstractContextAddObjects(){
		AbstractContextModel model = new AbstractContextModel();
		Set s = new HashSet();
		try {
			model.addObjects(s);
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
