/**
 * Test implementation for testing attribute exploration in partial contexts.
 * @author Baris Sertkaya
 */
package input_based;

import junit.framework.TestCase;
import statement_coverage.NoExpertPartial;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.action.StartExplorationAction;
import de.tudresden.inf.tcs.fcalib.utils.ListSet;

/*
 * FCAlib: An open-source extensible library for Formal Concept Analysis 
 *         tool developers
 * Copyright (C) 2009  Baris Sertkaya
 *
 * This file is part of FCAlib.
 * FCAlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCAlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCAlib.  If not, see <http://www.gnu.org/licenses/>.
 */

public class TestPartialContext extends TestCase {

	public TestPartialContext() {
	}
	
	public void testPartialContext() {
		BasicConfigurator.configure();

		PartialContext<String, String, PartialObject<String, String>> context = new PartialContext<>();
		NoExpertPartial<String> expert = new NoExpertPartial<>(context);

		context.addAttribute("a");
		context.addAttribute("b");
		context.addAttribute("c");
		context.addAttribute("d");
		System.out.println("Attributes: " + context.getAttributes());

		expert.addExpertActionListener(context);
		context.setExpert(expert);

		StartExplorationAction<String, String, PartialObject<String, String>> action = new StartExplorationAction<>();
		action.setContext(context);
		expert.fireExpertAction(action);
	}

	@Test(expected = NullPointerException.class)
	public void testAddNullObject() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		try{
		context.addObject(null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddObject() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		assertEquals(o,context.getObject("id"));
	}
	
	@Test
	public void testGetObject(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		o.getIdentifier().equals("id");
		context.getObject("id");
	}
	
	@Test
	public void testGetObjectNull(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.getObject(null);
	}
	
	@Test
	public void testGetBases(){
		PartialContext context = new PartialContext();
		context.getStemBase();
		context.getDuquenneGuiguesBase();
	}
	
	@Test(expected = NullPointerException.class)
	public void testRemoveObject() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject id = new PartialObject("id");
		context.getExpert();
		try {
			context.addObject(id);
			context.removeObject(id);
			context.removeObject("add");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testRemoveObjectNull(){
		PartialContext context = new PartialContext();
		try {
			PartialObject id = new PartialObject("id");
			context.addObject(id);
			context.removeObject(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	@Test
	public void testAddAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
	}
	
	@Test
	public void testRemoveAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
		context.removeAttributeFromObject(attribute, id);
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddAttributeToObjectNull() throws IllegalAttributeException, IllegalObjectException{
		PartialContext context = new PartialContext();
		try{
		context.addAttributeToObject(null,null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testRemoveAttributeToObjectNull() throws IllegalAttributeException, IllegalObjectException{
		PartialContext context = new PartialContext();
		try{
		context.removeAttributeFromObject(null, null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testObjectHasAttribute(){
		PartialContext context = new PartialContext();
		PartialObject obj = new PartialObject("id");
		Object attribute = "attr";
		context.objectHasAttribute(obj, attribute);
	}
	
	@Test(expected = NullPointerException.class)
	public void testObjectHasAttributeNull(){
		PartialContext context = new PartialContext();
		try{
		context.objectHasAttribute(null, null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testRefutes(){
		PartialContext context = new PartialContext();
		FCAImplication imp = null;
		context.refutes(imp);
	}
	
	@Test
	public void testDoublePrime(){
		PartialContext context = new PartialContext();
		Set x = new HashSet();
		context.doublePrime(x);
	}
	
	@Test
	public void testDoublePrimeNull(){
		PartialContext context = new PartialContext();
		context.doublePrime(null);
	}
	
	@Test
	public void testAddandRemoveObjectAtIndex() throws IllegalObjectException{
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("index");
		context.addObject(o);
		context.getObjectAtIndex(0);
	}
	
	@Test
	public void testRemovedObjectIsSameAsAdded(){
		PartialContext context = new PartialContext();
		PartialObject o = new PartialObject("id");
		try {
			context.addObject(o);
			context.removeObject("id");
			context.getObjectCount();
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.clearObjects();
	}
}
