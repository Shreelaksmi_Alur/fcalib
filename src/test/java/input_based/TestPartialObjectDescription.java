package input_based;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;
import de.tudresden.inf.tcs.fcalib.PartialObjectDescription;

public class TestPartialObjectDescription {
	
	@Test
	public void testFullObjectAddAttribute() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
	}
	
	@Test
	public void testFullObjectAddAttributeNull() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(null);
	}
	
	@Test
	public void testAddNegatedAttribute() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addNegatedAttribute(attribute);
	}
	
	@Test
	public void testAddNegatedAttributeNull() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		context.addNegatedAttribute(null);
	}
	
	@Test
	public void testContainsNegatedAttribute() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		Set attrs = new HashSet<>();
		context.addNegatedAttribute(attribute);
		context.containsNegatedAttribute(attribute);
	}	
	
	@Test
	public void testContainsNegatedAttributeNull() {
		PartialObjectDescription context = new PartialObjectDescription();
		Object attribute = new HashSet<>();
		Set attrs = new HashSet<>();
		context.addNegatedAttribute(attribute);
		context.containsNegatedAttribute(null);
	}
}
