/**
 * Test implementation for testing attribute exploration in partial contexts.
 * @author Baris Sertkaya
 */
package input_based;

import junit.framework.TestCase;
import statement_coverage.NoExpertFull;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.BasicConfigurator;
import org.junit.Ignore;
import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalAttributeException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalContextException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalExpertException;
import de.tudresden.inf.tcs.fcaapi.exception.IllegalObjectException;
import de.tudresden.inf.tcs.fcalib.FormalContext;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.PartialContext;
import de.tudresden.inf.tcs.fcalib.PartialObject;
import de.tudresden.inf.tcs.fcalib.action.StartExplorationAction;

/*
 * FCAlib: An open-source extensible library for Formal Concept Analysis 
 *         tool developers
 * Copyright (C) 2009  Baris Sertkaya
 *
 * This file is part of FCAlib.
 * FCAlib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FCAlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FCAlib.  If not, see <http://www.gnu.org/licenses/>.
 */

public class TestFormalContext extends TestCase {

	public TestFormalContext() {
	}
	
	public void testFormalContext() {
		BasicConfigurator.configure();

		FormalContext<String, String> context = new FormalContext<>();
		NoExpertFull<String> expert = new NoExpertFull<>(context);

		context.addAttribute("a");
		context.addAttribute("b");
		context.addAttribute("c");

		System.out.println("Attributes: " + context.getAttributes());

		expert.addExpertActionListener(context);
		context.setExpert(expert);

		StartExplorationAction<String, String, FullObject<String, String>> action = new StartExplorationAction<>();
		action.setContext(context);
		expert.fireExpertAction(action);
	}
	

	@Test(expected = NullPointerException.class)
	public void testAddNullObject() throws IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		try{
		context.addObject(null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddObject() throws IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		context.addObject(o);
		assertEquals(o,context.getObject("id"));
	}
	
	@Test
	public void testGetObject(){
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		o.getIdentifier().equals("id");
		context.getObject("id");
	}
	
	@Test
	public void testGetObjectNull(){
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		context.getObject(null);
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testRemoveObject(){
		FormalContext context = new FormalContext();
		FullObject id = new FullObject("id");
		try {
			context.addObject(id);
			context.removeObject(id);
			context.removeObject("add");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testRemoveObjectNull() throws IllegalObjectException{
		FormalContext context = new FormalContext();
		try {
			FullObject id = new FullObject("id");
			context.addObject(id);
			context.removeObject(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testRemoveObjectIfNotRemoved(){
		FormalContext context = new FormalContext();
		FullObject id = new FullObject("id");
		try {
			context.removeObject(id);
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRemoveObjectWithId() throws IllegalObjectException{
		FormalContext context = new FormalContext();
		String id = "";
		FullObject o = new FullObject(id);
		context.addObject(o);
		context.removeObject(id);
	}

	
	@Test
	public void testAddAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		context.addObject(o);
		context.getIntents();
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
	}
	
	@Test(expected = IllegalObjectException.class)
	public void testAddAttributeToObjectDoesntContainsId() {
		FormalContext context = new FormalContext();
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		try {
			context.addAttributeToObject(attribute,id);
		} catch (IllegalAttributeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddAndRemoveAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
		context.removeAttributeFromObject(attribute, id);
	}
	
	@Test
	public void testRemoveAttributeToObject() throws IllegalAttributeException, IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		context.addObject(o);
		Object attribute = "attr";
		Object id = "id";
		context.addAttribute(attribute);
		context.addAttributeToObject(attribute,id);
		context.removeAttributeFromObject(attribute, id);
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddAttributeToObjectNull() throws IllegalAttributeException, IllegalObjectException{
		FormalContext context = new FormalContext();
		try{
		context.addAttributeToObject(null,null);
		}catch (NullPointerException e){
		e.printStackTrace();
		}
	}
	
	@Test(expected = NullPointerException.class)
	public void testRemoveAttributeToObjectNull() throws IllegalAttributeException, IllegalObjectException{
		FormalContext context = new FormalContext();
		try{
		context.removeAttributeFromObject(null, null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testObjectHasAttribute(){
		FormalContext context = new FormalContext();
		FullObject obj = new FullObject("id");
		Object attribute = "attr";
		context.objectHasAttribute(obj, attribute);
	}
	
	@Test(expected = NullPointerException.class)
	public void testObjectHasAttributeNull(){
		FormalContext context = new FormalContext();
		try{
		context.objectHasAttribute(null, null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testClosure(){
		FormalContext context = new FormalContext();
		Set x = new HashSet<>();
		context.closure(x);
	}
	
	@Test
	public void testClosureNull(){
		FormalContext context = new FormalContext();
		context.closure(null);
	}
	
	@Test
	public void testIsClosed(){
		FormalContext context = new FormalContext();
		Set x = new HashSet<>();
		context.allClosures();
		context.isClosed(x);
	}
	
	@Test (expected = NullPointerException.class)
	public void testIsClosedNull(){
		FormalContext context = new FormalContext();
		try{
		context.isClosed(null);
		}catch (NullPointerException e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAddandRemoveObjectAtIndex() throws IllegalObjectException{
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("index");
		context.addObject(o);
		context.getObjectAtIndex(0);
	}
	
	@Test
	public void testRemovedObjectIsSameAsAdded(){
		FormalContext context = new FormalContext();
		FullObject o = new FullObject("id");
		try {
			context.addObject(o);
			context.removeObject("id");
		} catch (IllegalObjectException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.clearObjects();
	}
	
}
