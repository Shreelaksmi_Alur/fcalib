package input_based;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.PartialObject;

public class TestPartialObject {

	@Test
	public void testSetNameEmptyString() {
		Set attrs = new HashSet<>();
		PartialObject objectContext = new PartialObject("id",attrs);
		PartialObject context = new PartialObject("id");
		context.setName("thanos");
	}
	
	@Test
	public void testSetNameNullString() {
		PartialObject context = new PartialObject("id");
		context.setName(null);
	}
	
	@Test
	public void testSetNameStringNotEmpty() {
		Set attrs = new HashSet<>();
		Set negatedAttrs = new HashSet<>();
		PartialObject context = new PartialObject("id",attrs,negatedAttrs);
		context.setName("wolverine");
	}
	
	@Test
	public void testGetNameShouldBeSameAsSet() {
		PartialObject context = new PartialObject("id");
		context.toString();
		context.setName("stan lee");
		assertEquals("stan lee",context.getName());
	}
	
	@Test
	public void testGetNameShouldBeSameAsSetEmpty() {
		PartialObject context = new PartialObject("id");
		context.toString();
		context.setName("");
		assertEquals("",context.getName());
	}
}
