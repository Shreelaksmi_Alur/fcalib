package input_based;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.Test;

import de.tudresden.inf.tcs.fcaapi.FCAImplication;
import de.tudresden.inf.tcs.fcalib.FullObject;
import de.tudresden.inf.tcs.fcalib.Implication;

public class TestFullObject {
	
	public FullObject object = new FullObject("",new HashSet<>());

	@Test
	public void testSetNameEmptyString() {
		FullObject context = new FullObject("id");
		context.setName("");
	}
	
	@Test
	public void testSetNameNullString() {
		FullObject context = new FullObject("id");
		context.setName(null);
	}
	
	@Test
	public void testSetNameStringNotEmpty() {
		FullObject context = new FullObject("id");
		context.setName("yoda");
	}
	
	@Test
	public void testGetNameShouldBeSameAsSet() {
		FullObject context = new FullObject("id");
		context.toString();
		context.setName("stan lee");
		assertEquals("stan lee",context.getName());
	}
	
	@Test
	public void testGetNameShouldBeSameAsSetEmpty() {
		FullObject context = new FullObject("id");
		context.toString();
		context.setName("");
		assertEquals("",context.getName());
	}
}
