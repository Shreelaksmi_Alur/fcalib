package input_based;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import de.tudresden.inf.tcs.fcalib.FullObjectDescription;

public class TestFullObjectDescription {

	@Test
	public void testFullObjectAddAttribute() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
	}
	
	@Test
	public void testFullObjectAddAttributeNull() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(null);
	}
	
	@Test
	public void testObjectClone() throws CloneNotSupportedException {
		FullObjectDescription context = new FullObjectDescription();
		context.clone();
	}
	
	@Test
	public void testAddAttributes() {
		FullObjectDescription context = new FullObjectDescription();
		Set attrs = new HashSet<>();
		context.addAttributes(attrs);
	}
	
	@Test(expected = NullPointerException.class)
	public void testAddAttributesNull() {
		FullObjectDescription context = new FullObjectDescription();
		Set attrs = new HashSet<>();
		context.addAttributes(null);
	}

	@Test
	public void testRemoveAttribute() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
		context.removeAttribute(attribute);
	}
	
	@Test
	public void testRemoveAttributeNull() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
		context.removeAttribute(null);
	}
	
	@Test
	public void testContainsAttribute() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		Set attrs = new HashSet<>();
		context.addAttribute(attribute);
		context.containsAttribute(attribute);
		context.containsAttributes(attrs);
	}	
	
	@Test
	public void testContainsAttributeNull() {
		FullObjectDescription context = new FullObjectDescription();
		Object attribute = new HashSet<>();
		context.addAttribute(attribute);
		context.containsAttribute(null);
	}

}
